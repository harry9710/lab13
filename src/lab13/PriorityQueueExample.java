/* Implement priority queue using ArrayList, LinkedList or Array. Implement methods for:
	+ poll
	+ peek
	+ add(String value, Integer score)
*/

package lab13;
import java.util.*;

public class PriorityQueueExample {
	public static void main(String[] args) {
		/*************************ARRAYLIST PART*************************/
		//Create comparator based on the size of the ArrayList
		Comparator<ArrayList<Integer>> aListComparator = Comparator.comparingInt(ArrayList<Integer>::size);
		PriorityQueue<ArrayList<Integer>> aLists = new PriorityQueue<>(aListComparator);

		//Create ArrayLists then add them into the Queue
		ArrayList<Integer> x0 = new ArrayList<>(Arrays.asList(0));
		ArrayList<Integer> x1 = new ArrayList<>(Arrays.asList(3, 4));
		ArrayList<Integer> x2 = new ArrayList<>(Arrays.asList(1, 5, 8));
		ArrayList<Integer> x3 = new ArrayList<>(Arrays.asList(9));		
		aLists.add(x0);	//add method
		aLists.add(x1);
		aLists.add(x2);
		aLists.add(x3);	
		
		//Print result
		System.out.println("Remove the first element of queue: " + aLists.poll());	//poll method
		System.out.println("First element after remove: " + aLists.peek());	//peek method
		System.out.print("Order by size: ");
		while (!aLists.isEmpty()) {
			System.out.print(aLists.remove() + " ");
		}
		System.out.println("\n");
		
		/*************************LINKEDLIST PART*************************/
		//Create comparator based on the first element of the LinkedList
		Comparator<LinkedList<Integer>> lListComparator = Comparator.comparingInt(LinkedList<Integer>::getFirst);
		PriorityQueue<LinkedList<Integer>> lLists = new PriorityQueue<>(lListComparator);
		
		//Create LinkedLists then add them into the Queue
		LinkedList<Integer> y0 = new LinkedList<>(Arrays.asList(0));
		LinkedList<Integer> y1 = new LinkedList<>(Arrays.asList(3, 4));
		LinkedList<Integer> y2 = new LinkedList<>(Arrays.asList(1, 5, 8));
		LinkedList<Integer> y3 = new LinkedList<>(Arrays.asList(9));
		lLists.add(y0);	//add method
		lLists.add(y1);
		lLists.add(y2);
		lLists.add(y3);
		
		//Print result
		System.out.println("Remove the first element of queue: " + lLists.poll());	//poll method
		System.out.println("First element after remove: " + lLists.peek());	//peek method
		System.out.print("Order by first element: ");
		while (!lLists.isEmpty()) {
			System.out.print(lLists.remove() + " ");
		}
		System.out.println("\n");
		
		/*************************ARRAY PART*************************/
		//Create comparator based on the last element of the Array
		PriorityQueue<int[]> arrays = new PriorityQueue<int[]>((a,b) -> a[a.length-1] - b[b.length-1]);
		
		//Create LinkedLists then add them into the Queue
	    int[] z0 = {0};
	    int[] z1 = {3, 4};
	    int[] z2 = {1, 5, 8};
	    int[] z3 = {9};
	    arrays.add(z0);	//add method
	    arrays.add(z1);
	    arrays.add(z2);
	    arrays.add(z3);
	    
	    //Print result
		System.out.println("Remove the first element of queue: " + Arrays.toString(arrays.poll()));	//poll method
		System.out.println("First element after remove: " + Arrays.toString(arrays.peek()));	//peek method
		System.out.print("Order by last element: ");
	    while (arrays.size() != 0)
	    {
	        System.out.print(Arrays.toString(arrays.remove()) + " ");
	    }
		System.out.println("\n");
	}
}
